#!/bin/bash

# This script is like cat for all files in directory
#
# cats - Concatenates all files in a specified folder or the current folder,
#        inserting the filename before each file's contents.
# Usage: cats [folder]
#   [folder]: Optional. The folder containing the files to concatenate.
#             If not provided, the current folder is assumed.

# If no folder is provided, assume the current folder
folder="${1:-.}"

# Check if the folder exists
if [ ! -d "$folder" ]; then
    echo "Error: Folder '$folder' not found."
    exit 1
fi

# Change directory to the specified folder
cd "$folder" || exit

# Loop through each file in the folder
for file in *; do
    # Check if it's a file
    if [ -f "$file" ]; then
        # Print separator and filename
        echo "-------------------------------------------"
        echo "-- File: $file"
        echo "-------------------------------------------"
        # Display file contents
        cat "$file"
        # Add EOF marker
        echo -e "\n<EOF>\n"
    fi
done
