#!/bin/bash

# This script launches multiple gnome-terminal windows at specified positions and sizes on the screen.
# It is designed to automatically arrange terminals across different workspaces for a streamlined workflow setup.
# Dependencies: gnome-terminal, wmctrl

LOGFILE=~/terminals-log.txt  # Log file to track the operations and status of terminal setups.

# Log the initiation of the terminal setup process.
echo "$(date) - Starting terminal setup" >> $LOGFILE

# Function to start a gnome-terminal with specific geometry and optionally run a command
# Arguments:
#   x_pos - X position on the screen
#   y_pos - Y position on the screen
#   width - Width of the terminal in columns
#   height - Height of the terminal in rows
#   workspace - Workspace number to move the terminal to
#   command - Optional; Command to execute in the terminal
start_terminal() {
    x_pos=$1
    y_pos=$2
    width=$3
    height=$4
    workspace=$5
    command=$6

    # Start gnome-terminal with specific geometry. If a command is provided, execute it.
    if [ -z "$command" ]; then
        gnome-terminal --geometry=${width}x${height}+${x_pos}+${y_pos} &
    else
        gnome-terminal --geometry=${width}x${height}+${x_pos}+${y_pos} --name=$command --title=$command -- $command &
    fi
    sleep 1  # Wait for the terminal to initialize.


    # kate, python
    if [ -n "$command" ]; then     # Here, -n tests if the string is not null (i.e., not empty).
        wmctrl -r $command -t $workspace
        sleep 1
    fi

    # Use wmctrl to move the active window to the specified workspace.
    wmctrl -r :ACTIVE: -t $workspace
    echo "$(date) - Terminal positioned at ${x_pos}, ${y_pos}, size ${width}x${height}, on workspace $workspace" >> $LOGFILE
}

# Launch and position terminals:
start_terminal 0 0 80 20 3    # Terminal 1 on workspace 4 # workspaces are indexed here as 0..3
start_terminal 730 0 80 20 3  # Terminal 2 on workspace 4
start_terminal 0 380 80 20 3  # Terminal 3 on workspace 4
start_terminal 730 380 80 20 3 "python3"  # Terminal 4 starts Python on workspace 4
start_terminal 0 0 80 20 2 "kate"  # Terminal 3 starts kate on workspace 3


# Log completion of the terminal setup process.
echo "$(date) - Terminal setup completed" >> $LOGFILE
